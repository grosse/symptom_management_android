package client;

/**
 * Created by eric on 11/6/2014.
 */
/*
Taken from mobile14 class Jules White
This defines a contract for a exectution "Future" or continuation
*/




public interface TaskCallback<T> {

    public void success(T result);

    public void error(Exception e);

}