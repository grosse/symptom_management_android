package client;

import org.grosse2011.symptom.ImageStatus;

import java.util.Collection;

import business_objects.CheckIn;
import business_objects.CheckInDto;
import business_objects.DoctorDto;
import business_objects.PainMedicationDto;
import business_objects.PatientDto;
import business_objects.PrescriptionDto;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.mime.TypedFile;

//import business_objects.CheckInKill;
//import business_objects.Doctor;
//import business_objects.PatientKill;

/**
 * Created by eric on 10/16/2014.
 * Here is the API used by retrofit to talk to the REST server
 */
public interface  SymptomSvcApi {

    public static final String PATIENT_SVC_PATH = "/patients";
    public static final String TOKEN_PATH = "/oauth/token";
    public static final String DOCTOR_SVC_PATH = "/doctors";
    public static final String PAIN_MEDICATION_SVC_PATH = "/painMedications";
    public static final String PRESCRIPTION_BY_PATIENT = PATIENT_SVC_PATH + "/{id}/prescriptions";
    public static final String CHECKINS_BY_PATIENT = PATIENT_SVC_PATH + "/{id}/checkIns";
    public static final String PATIENTS_TO_ALERT_BY_DOCTOR = DOCTOR_SVC_PATH + "/{id}/patientsToAlert/{hours}";
    public static final String PATIENTS_BY_DOCTOR_PATH= DOCTOR_SVC_PATH + "/{id}/patients";
    public static final String IMAGE_CHECKIN_PATH = "/checkIns/{id}/images";
    public static final String CHECKIN_SVC_PATH = "/checkIns";
    public static final String GET_CHECKINS_BY_PATIENT_ORDER_BY_DATE = PATIENT_SVC_PATH + "/{id}/getCheckInsOrderByDate";

    @GET(SymptomSvcApi.PATIENTS_TO_ALERT_BY_DOCTOR)
    public Collection<PatientDto> getPatientsToAlert(@Path("id") long id, @Path("forLastNHours") int hours);

    @GET(SymptomSvcApi.CHECKINS_BY_PATIENT)
    public Collection<CheckInDto> getCheckInsByPatient(@Path("id") long id);

    @POST(SymptomSvcApi.CHECKINS_BY_PATIENT)
    public CheckInDto addCheckInByPatient(@Path("id") long id, @Body CheckInDto checkIn);

    @POST(SymptomSvcApi.PRESCRIPTION_BY_PATIENT)
    public boolean createPrescriptionByPatient(@Path("id") long id);

    @PUT(SymptomSvcApi.PRESCRIPTION_BY_PATIENT)
    public PatientDto upsertPrescriptionForPatient(@Path("id") long id, @Body PrescriptionDto prescription, @Path("side") String side);

    @GET(SymptomSvcApi.PRESCRIPTION_BY_PATIENT)
    public PrescriptionDto getPrescriptionByPatient(@Path("id") long id);

    @GET(SymptomSvcApi.PRESCRIPTION_BY_PATIENT + "/side")
    public PrescriptionDto getPrescriptionByPatientSide(@Path("id") long id);


    @GET(PAIN_MEDICATION_SVC_PATH)
    public Collection<PainMedicationDto> getPainMedications();

    @GET(value = DOCTOR_SVC_PATH)
    public Collection<DoctorDto> getDoctors();

    @GET(value = PATIENTS_BY_DOCTOR_PATH)
    public Collection<PatientDto> getPatientsByDoctor(@Path("id") long id);


    @GET(CHECKIN_SVC_PATH)
    public Collection<CheckIn> getCheckInList();

    @POST(CHECKIN_SVC_PATH)
    public boolean addCheckIn(@Body CheckIn c);

    @Multipart
    @POST(IMAGE_CHECKIN_PATH)
    public ImageStatus addImageToCheckIn(@Path("id") long id, @Part("data") TypedFile imageData);

    @GET(IMAGE_CHECKIN_PATH)
    Response getImageForCheckIn(@Path("id") long id);

    @GET(SymptomSvcApi.GET_CHECKINS_BY_PATIENT_ORDER_BY_DATE)
    public Collection<CheckInDto> getCheckInsByPatientOrderByDate(@Path("id") long id);

    @GET(value = DOCTOR_SVC_PATH + "/{id}")
    public DoctorDto getDoctor(@Path("id") long id);

    @GET(value = PATIENT_SVC_PATH + "/{id}")
    public PatientDto getPatient(@Path("id") long id);


}