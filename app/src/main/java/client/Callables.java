package client;

import org.grosse2011.symptom.EditPrescriptionActivity;
import org.grosse2011.symptom.Misc;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.concurrent.Callable;

import business_objects.CheckInDto;
import business_objects.DoctorDto;
import business_objects.PatientDto;
import business_objects.PrescriptionDto;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by eric on 11/8/2014.
These are a set of nested Callables used by CallableTask in order to communicate with Rest API's
 off the main UI thread using AyncTask.  Each callable implements a call method which is run
 as in the Background
  */

public class Callables {
    // Each of these callables is used to a inovke rest API on a server
    // We cannot call network code on main UI thread in Android, so we will use Callable and CallableTask
    // together to perform call on a background thread

    public static class getCheckInsByPatient implements Callable<Collection<CheckInDto>> {
        SymptomSvcApi svc;
        long patientId;

        public getCheckInsByPatient(long patientId, SymptomSvcApi svc) {
             this.svc = svc;
             this.patientId = patientId;
        }

        public Collection<CheckInDto> call() throws Exception {
            return this.svc.getCheckInsByPatient(patientId);
        }
    }

    public static class getCheckInsByPatientOrderByDate implements Callable<Collection<CheckInDto>> {
        SymptomSvcApi svc;
        long patientId;

        public getCheckInsByPatientOrderByDate(long patientId, SymptomSvcApi svc) {
            this.svc = svc;
            this.patientId = patientId;
        }

        public Collection<CheckInDto> call() throws Exception {
            return this.svc.getCheckInsByPatientOrderByDate(patientId);
        }
    }

    public static class getPatientsByDoctorCallable implements Callable<Collection<PatientDto>> {
        SymptomSvcApi svc;
        long doctorId;

        public getPatientsByDoctorCallable(long doctorId, SymptomSvcApi svc) {
            this.svc = svc;
            this.doctorId = doctorId;
        }

        public Collection<PatientDto> call() throws Exception {
            return this.svc.getPatientsByDoctor(doctorId);
        }
    }


    public static class getDoctorsCallable implements Callable<Collection<DoctorDto>> {
        SymptomSvcApi svc;

        public getDoctorsCallable(SymptomSvcApi svc) {
          this.svc = svc;
        }

        public Collection<DoctorDto> call() throws Exception {
            return this.svc.getDoctors();
        }
    }

    public static class getPrescriptionByPatientCallable implements Callable<PrescriptionDto> {
        long patientId;
        SymptomSvcApi svc;

        public getPrescriptionByPatientCallable(SymptomSvcApi svc, long patientId) {
            this.patientId = patientId;
            this.svc = svc;
        }

        public PrescriptionDto call() throws Exception {
            PrescriptionDto prescriptionDto = this.svc.getPrescriptionByPatientSide(this.patientId);
            return prescriptionDto;
        }
    }


    public static class upsertPrescriptionForPatient implements Callable<PatientDto> {
        EditPrescriptionActivity activity;
        SymptomSvcApi svc;
        //SymptomSvcApi svc;
        public upsertPrescriptionForPatient(EditPrescriptionActivity activity) {
            this.activity = activity;
            //this.svc = this.activity.getSvc();
            this.svc = this.activity.getSvc();
        }

        public PatientDto call() throws Exception {
            PrescriptionDto prescription = activity.getNewPrescription();
            String side = Misc.prescriptionToSide(prescription);
            return this.svc.upsertPrescriptionForPatient(activity.getPatientId(), prescription, side);
        }
    }

    public static class addCheckInByPatient implements Callable<CheckInDto> {
        long patientId;
        CheckInDto checkIn;
        SymptomSvcApi svc;

        public addCheckInByPatient(long patientId, CheckInDto checkIn, SymptomSvcApi svc) {
           this.patientId = patientId;
           this.checkIn = checkIn;
           this.svc = svc;
        }

        public CheckInDto call() throws Exception {
           return svc.addCheckInByPatient(patientId, checkIn);
        }
    }

    public static class addImageToCheckIn implements Callable<InputStream> {
        long checkInId;
        TypedFile image;
        SymptomSvcApi svc;

        public addImageToCheckIn(long checkInId, TypedFile image, SymptomSvcApi svc) {
            this.checkInId = checkInId;
            this.image = image;
            this.svc = svc;
        }

        public InputStream call() throws Exception {
           svc.addImageToCheckIn(checkInId, image);
           Response response = svc.getImageForCheckIn(checkInId);
           //byte[] retrievedFile = null;
           try {
                // Return file as Input byte stream
                return response.getBody().in();
          } catch (IOException e) {
                return null;
            }
        }
    }

    public static class getImageForCheckIn implements Callable<InputStream> {
        long checkInId;
        TypedFile image;
        SymptomSvcApi svc;

        public getImageForCheckIn(long checkInId,  SymptomSvcApi svc) {
           this.checkInId = checkInId;
           this.svc = svc;
        }

        public InputStream call() throws Exception {

            Response response = svc.getImageForCheckIn(checkInId);
            byte[] retrievedFile = null;
             try {
                 return response.getBody().in();
            } catch (IOException e) {
                return null;
        }
    }
}

    public static class getAlerts implements Callable<Collection<PatientDto>> {
        long doctorId;
        long date;
        int forLastNHours;
        SymptomSvcApi svc;

        public getAlerts(long doctorId, int forLastNHours, SymptomSvcApi svc) {
            this.doctorId = doctorId;
            this.forLastNHours = forLastNHours;
            this.svc = svc;
        }
        // This call find patients who meet alerting critiraa over the last
        public Collection<PatientDto> call() throws Exception {
            Collection<PatientDto> patientDto = svc.getPatientsToAlert(doctorId, forLastNHours);
            return patientDto;
        }
    }
}





