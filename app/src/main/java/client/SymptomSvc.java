package client;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.bind.DateTypeAdapter;

import java.util.Date;

import oauth.SecuredRestBuilder;
import retrofit.RestAdapter;
import retrofit.client.ApacheClient;
import retrofit.converter.GsonConverter;
import unsafe.EasyHttpClient;

/**
 * Created by eric on 11/6/2014.
 Stolen from Jules White's oath Video Svc example in mobile dev class

 */

public class SymptomSvc {

    public static final String CLIENT_ID = "mobile";

    private static SymptomSvcApi symptomSvc_ = null;
    private static EasyHttpClient client_ = null;


    public static synchronized SymptomSvcApi getCurrentSvc() {
        return symptomSvc_;
    }
    public static synchronized SymptomSvcApi init(String server, String user, String pass) {

       return init_secure_(server, user, pass);
    }

    /* Create the service endpoint to be used.  This service endpoint will use SSL and OAUTH2 using
    the password grant flow.  The SecuredRestBuilder is an adapter which knows how to get an OAUTH bearer token initially
    and add the bearer token to all subsequent requests.  SecuredRestBuilder uses the EastHttpClient class
    which can use SSL using an untrusted, self signed certificate.
         */
    public static synchronized SymptomSvcApi init_secure_(String server, String user, String pass) {



            symptomSvc_ = new SecuredRestBuilder()
                    .setLoginEndpoint(server + SymptomSvcApi.TOKEN_PATH)
                    .setUsername(user)
                    .setPassword(pass)
                    .setClientId(CLIENT_ID)
                    .setClient(
                            new ApacheClient(new EasyHttpClient()))
                    .setEndpoint(server).setLogLevel(RestAdapter.LogLevel.FULL).build()
                    .create(SymptomSvcApi.class);

            return symptomSvc_;
        }


    /* Non-secured rest adaptor for testing */

    public static synchronized SymptomSvcApi init_(String server)              {

        Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new DateTypeAdapter())
                        .create();



        symptomSvc_ = new RestAdapter.Builder()
                .setEndpoint(server).setConverter(new GsonConverter(gson)).build().create(SymptomSvcApi.class);
        return symptomSvc_;
    }
}