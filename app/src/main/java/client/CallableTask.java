package client;

/**
 * Created by eric on 11/6/2014.
 */

import android.os.AsyncTask;
import android.util.Log;
import java.util.concurrent.Callable;


public class CallableTask<T> extends AsyncTask<Void,Double,T> {

    private static final String TAG = CallableTask.class.getName();

    public static <V> void invoke(Callable<V> call, TaskCallback<V> callback){
        new CallableTask<V>(call, callback).execute();
    }
    private Callable<T> callable_;
    private TaskCallback<T> callback_;
    // remember any exceptions
    private Exception error_;

   // Takes a Callable with a call method which will be run in the backgound on this AsyncTask
   // Takes and a callback or "Future" to be invoked onPost execution

    public CallableTask(Callable<T> callable, TaskCallback<T> callback) {
        callable_ = callable;
        callback_ = callback;
    }

    @Override
    protected T doInBackground(Void... ts) {
        T result = null;
        try{
            result = callable_.call();
        } catch (Exception e){
            Log.e(TAG, "Error invoking callable in AsyncTask callable: "+callable_, e);
            error_ = e;
        }
        return result;
    }
    // call error, passing the Exception on callback "Future" if an exception occured,
    // otherwise call success
    @Override
    protected void onPostExecute(T r) {
        if(error_ != null){
            callback_.error(error_);
        }
        else {
            callback_.success(r);
        }
    }
}

