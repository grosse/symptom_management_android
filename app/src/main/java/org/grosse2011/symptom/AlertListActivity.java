package org.grosse2011.symptom;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collection;

import business_objects.PatientDto;
import client.CallableTask;
import client.Callables;
import client.SymptomSvc;
import client.SymptomSvcApi;
import client.TaskCallback;

/* This Activity show all the alerts for a doctor */

// TODO move the TaskCallback into callables and remove the interface implementation
public class AlertListActivity extends ListActivity implements TaskCallback<Collection<PatientDto>> {
    public static final int ALERTS_HOURS=24;
    ArrayList<PatientDto> mPatients;
    Context mContext;
    long mDoctorId = -1;
    String mFirstName;
    String mLastName;


    private final SymptomSvcApi svc = SymptomSvc.getCurrentSvc(); //SymptomSvc.init("http://10.0.2.2:8080/", "doctor0", "pass");

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mDoctorId = getIntent().getLongExtra(Misc.DOCTOR_ID_KEY, -1);
        this.mFirstName = getIntent().getStringExtra(Misc.DOCTOR_FIRST_NAME_KEY);
        this.mLastName  = getIntent().getStringExtra(Misc.DOCTOR_LAST_NAME_KEY);
        setTitle("Alerts for " + this.mFirstName + " " + mLastName + " M.D.");
        // Call to Rest API to get alerts for this doctor
        CallableTask.invoke(new Callables.getAlerts(mDoctorId, ALERTS_HOURS, svc), this);
           }
    // continuation "Future" of Asyntask Callable

    private void finishGetAlerts() {
       // Place the downloaded list of patients with alerts into the arrayAdapter
        ArrayAdapter<PatientDto> adapter = new ArrayAdapter<PatientDto>(this, R.layout.patient_list_item, this.mPatients);
        setListAdapter(adapter);
        mContext = this;
        ListView patientListView = getListView();
        patientListView.setTextFilterEnabled(true);
        patientListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                                                PatientDto curPatientDto = mPatients.get(position);
                                                Intent intent = new Intent(mContext, PatientOpsActivity.class);
                                                intent.putExtra(Misc.PATIENT_ID_KEY, curPatientDto.getId());
                                                intent.putExtra(Misc.PATIENT_FIRST_NAME_KEY, curPatientDto.firstName);
                                                intent.putExtra(Misc.PATIENT_LAST_NAME_KEY, curPatientDto.lastName);
                                                startActivity(intent);
                                            }
                                        }
        );
    }

    public void success(Collection<PatientDto> result) {
        this.mPatients = (ArrayList<PatientDto>) result;
        finishGetAlerts();
    }

    @Override
    public void error(Exception e) {

    }

}