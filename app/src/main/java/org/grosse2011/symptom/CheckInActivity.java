package org.grosse2011.symptom;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

import business_objects.CheckIn;
import business_objects.CheckInDto;
import business_objects.MedicationTakenDto;
import business_objects.PainMedicationDto;
import business_objects.PrescriptionDto;
import client.CallableTask;
import client.Callables;
import client.SymptomSvc;
import client.SymptomSvcApi;
import client.TaskCallback;

import static org.grosse2011.symptom.Misc.pad;

/*

This screen if for a patient to CheckIn with a dotor.
Patient will input their pain and eating status and answer questions
about whether they took their pain medications
 */


public class CheckInActivity extends Activity {
    private static final int TIME_DIALOG_ID = 0x0;
    private static final int DATE_DIALOG_ID = 0x2000;

    private static final int MEDIA_TYPE_IMAGE = 1;
    private static final int CAMERA_PIC_REQUEST = 1;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private static final String TITLE_PREFIX = "Checking In";
    private static final String IMAGE_CACHE_DIRECTORY="symptom";
    private static final String IMAGE_NAME_PREFIX = "IMG_";
    private CheckInDto mCheckIn = new CheckInDto();
    private static final String NOT_SET_TAG = "NOT SET";
    private boolean mPainSet = false;
    private boolean mEatingSet = false;
    private ListView mMedicationsTakenListView;
    private int mMinute;
    private int mMonth;
    private int mYear;
    private int mDay;
    private int mHour;
    private String mFirstName = null;
    private String mLastName = null;

    private String mTitle = TITLE_PREFIX;
    private Button mMedicationsButton;
    private Spinner mPainSpinner;
    private Spinner mIsEatingSpinner;

    private Uri mImageUri = null;
    private File mImageFile = null;
    private ArrayList<TimePickOnSetTimeListener> mTimePickSetTimeListeners;

    private final SymptomSvcApi mSvc = SymptomSvc.getCurrentSvc(); //SymptomSvc.init(Misc.SERVER_URL);
    Intent mRecievedIntent;
    long mPatientId;
    CheckInDto getCheckIn() { return this.mCheckIn; }
    public SymptomSvcApi getSvc() { return this.mSvc; }
    public String mImageFileName = null;
    public File getImageFile() { return this.mImageFile; }
    public Uri getImageUri() { return this.mImageUri; }
    public long getPatientId() { return this.mPatientId; }
    public boolean nameSet() { return !(mFirstName == null || mLastName == null);  }
    public void setName(String firstName, String lastName) {
        this.mFirstName = firstName;
        this.mLastName = lastName;
    }
    private TimePickOnSetTimeListener[] mTimeSetListener;
    private DatePickOnSetDateListener[] mDateSetListener;


    public  void setDateSetListener(DatePickOnSetDateListener dateSetListener, int idx) {
        // We need to keep and array of listeners for the date pickers so we know for
        // which medicationTaken we are setting the date for
        this.mDateSetListener[idx] = dateSetListener;
    }

    public DatePickOnSetDateListener getDateSetListener(int idx) {
        return this.mDateSetListener[idx];
    }
    public  void setTimeSetListener(TimePickOnSetTimeListener timeSetListener, int idx) {
        // We need to keep and array of listeners for the time pickers so we know for
        // which medicationTaken we are setting the time for
        this.mTimeSetListener[idx] = timeSetListener;
    }
    public TimePickOnSetTimeListener getTimeSetListener(int idx) {
        return this.mTimeSetListener[idx];
    }


    Collection<EditText> mDateEdits = new ArrayList<EditText>();
    Collection<EditText> mTimeEdits = new ArrayList<EditText>();
   // PrescriptionDto mPrescription;
    //ArrayList<MedicationTakenDto> mMedicationsTaken;
    Context mContext;

    PrescriptionDto mPrescription;
    Button mSendButton;
    Button mTakePhotoButton;

    /* Return all medicationsTaken for this checkIn for whichthe time is not set */

    public Collection<String> medicationsTakenWithNoTime() {
       ArrayList<String> painMeidcationsNames = new ArrayList<String>();
       for (MedicationTakenDto medicationTakenDto : this.mCheckIn.medicationsTaken) {

       if (medicationTakenDto.hasTaken == true && (medicationTakenDto.dateTaken == null || medicationTakenDto.timeTaken == null)) {
                painMeidcationsNames.add(medicationTakenDto.medication.name);
           }
        }
        return painMeidcationsNames;
    }
    /* Return all medicationsTaken for this checkin which hasTaken is set to false */

    public Collection<String> medicationsNotTaken() {
        ArrayList<String> painMedicationNames = new ArrayList<String>();

        for (MedicationTakenDto medicationTakenDto : this.mCheckIn.medicationsTaken) {
            if (medicationTakenDto.hasTaken == false) {
                painMedicationNames.add(medicationTakenDto.medication.name);
            }
        }
        return painMedicationNames;
    }


    @Override
    @SuppressWarnings("deprecation")
    // TODO figure out how new TimPckerDialog works
    protected Dialog onCreateDialog(int id) {
        // use ID as an index into Listener arrays
        // DateListener array is offset by DATE_DIALOG_ID
        if (id < DATE_DIALOG_ID) {
            return new TimePickerDialog(this, getTimeSetListener(id), mHour, mMinute, false);
        }
        else {
            return new DatePickerDialog(this, getDateSetListener(id - DATE_DIALOG_ID), mYear, mMonth, mDay);
        }

    }
    public static class TimePickOnSetTimeListener implements TimePickerDialog.OnTimeSetListener {

        MedicationTakenDto mMedicationTakenDto;
        int mHour;
        int mMinute;


        public TimePickOnSetTimeListener(MedicationTakenDto medicationTakenDto, int hour, int minutes) {
           // this.medTaken = medTaken;
            this.mMinute = minutes;
            this.mHour = hour;
            this.mMedicationTakenDto = medicationTakenDto;
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
               mMedicationTakenDto.setTimeTaken(pad(hourOfDay) + ":" + pad(minute));
               }
            }


    public static class DatePickOnSetDateListener implements DatePickerDialog.OnDateSetListener {

        MedicationTakenDto mMedicationTakenDto;
        int mYear;
        int mMonth;
        int mDay;


        public DatePickOnSetDateListener(MedicationTakenDto med, int year, int month, int day) {
            // this.medTaken = medTaken;
            this.mYear = year;
            this.mMonth = month;
            this.mDay = day;
            this.mMedicationTakenDto = med;
        }

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            mMedicationTakenDto.setDateTaken(mYear + "-" + pad(monthOfYear) + "-" + pad(dayOfMonth));
        }
    }
     /* This sets the listener for the send button, which the patient presses when
        they wish to upload data to the server.  The user must set the spinners for isEating
        and pain level before they can send data up to the server
      */
    private void setSendListener() {
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (! mEatingSet || !mPainSet) {
                    Toast.makeText(CheckInActivity.this, "You must set both pain level and eating", Toast.LENGTH_SHORT).show();
                    return;
                }
                // double check with the patient about medications they said they didn't take
                // before sending data to the server
                Collection<String> medicationsNotTaken = medicationsNotTaken();

                if (medicationsNotTaken != null) {
                    String msg = "You didn't take:\n\n";
                    for (String medicationNotTake : medicationsNotTaken) {
                        msg += medicationNotTake + "\n";
                    }
                    msg += "\nDo you still want to send?";
                    // Verifcation dialog, double check with user and
                    // data send if yes is chosen
                    AlertDialog reallySendDialog = new AlertDialog.Builder(CheckInActivity.this).setMessage(msg).setNegativeButton(
                            "No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                }
                            }).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            CheckInActivitySupport.sendCheckIn(CheckInActivity.this);
                        }
                    }).create();
                    reallySendDialog.show();
                } else {
                    CheckInActivitySupport.sendCheckIn(CheckInActivity.this);
                }
            }
        });
    }
    /*****************************************************/
    /* EMTRY POINT is here for the CheckInActivity  */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in);

       // ArrayList<PatientDto> patients;
        this.mRecievedIntent = getIntent();
        this.mPatientId = mRecievedIntent.getLongExtra(Misc.PATIENT_ID_KEY, -1);
        final Calendar calendar = Calendar.getInstance();
        String firstName = mRecievedIntent.getStringExtra(Misc.PATIENT_FIRST_NAME_KEY);
        String lastName = mRecievedIntent.getStringExtra(Misc.PATIENT_LAST_NAME_KEY);
        setName(firstName, lastName);

        if (nameSet()) {
            mTitle  += ", "  + firstName + " " + lastName;
        }
        setTitle(mTitle);
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mHour = calendar.get(Calendar.HOUR_OF_DAY);
        mMinute = calendar.get(Calendar.MINUTE);
        mDay = calendar.get(Calendar.DAY_OF_MONTH);
        // Get the prescription for this patient
        // TODO optimization, call this once and memoize it
        // perhaps use a Singleton to access and memoize
        CallableTask.invoke(

                new Callables.getPrescriptionByPatientCallable(mSvc, mPatientId)
                , new TaskCallback<PrescriptionDto>() {

                    @Override
                    public void success(PrescriptionDto prescription) {
                        continueOnCreate(prescription);
                    }

                    @Override
                    // Go back to login if there is a problem, this
                    // activity should have been started by a BroadCast receiver.
                    // This should not happen.
                    public void error(Exception e) {
                        Intent intent = new Intent(CheckInActivity.this, LoginScreenActivity.class);
                        startActivity(intent);
                    }
                });

    }
    /*
      This is the continuation "Future" of Aync call to get a Patients prescription
      Here we finish the CheckInActivity's lifecycle method onCreate
    */
    public void continueOnCreate(PrescriptionDto prescriptionDto) {
        mPrescription= prescriptionDto;

        mCheckIn.medicationsTaken = new ArrayList<MedicationTakenDto>();
        /* Build a list of potential Medications Taken
           We will ask the patient if they were taken, and set the hasTaken
           field accordingly
         */

        if (mPrescription != null && mPrescription.medications != null) {
            for (PainMedicationDto painMedicationDto : mPrescription.medications) {
                mCheckIn.medicationsTaken.add(new MedicationTakenDto(1, painMedicationDto));
            }
        }
        //Context context;
        mContext = this;
        mPainSpinner = (Spinner) findViewById(R.id.painSpinner);
        mIsEatingSpinner = (Spinner) findViewById(R.id.isEatingSpinner);
        mTakePhotoButton = (Button) findViewById(R.id.CheckInAddPhotoButton);
        mMedicationsButton = (Button) findViewById(R.id.CheckInAddMedicationsButton);
        mMedicationsTakenListView = (ListView) findViewById(R.id.medicationsTakenListView);

        mSendButton = (Button) findViewById(R.id.CheckInSendButton);
        setSendListener();

        mMedicationsButton.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
               askPatientAboutTakingMedications();
            }
        });

        mTakePhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePhoto();
            }
        });
        mPainSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                String pain = parent.getItemAtPosition(pos).toString();
                if (pain.equals(NOT_SET_TAG)) return;
                mCheckIn.pain = CheckIn.Pain.valueOf(pain);
                mPainSet = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mIsEatingSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                String isEating = parent.getItemAtPosition(pos).toString();
                if (isEating.equals(NOT_SET_TAG)) return;
                mCheckIn.isEating = CheckIn.StoppedEating.valueOf(isEating);

                // Put this last
                 mEatingSet = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.check_in, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    // return a uri associated with a file
      private static Uri getOutputMediaFileUri(File file){
        Uri uri = null;
        //File f = getOutputMediaFile(type);
        if (file != null)  uri = Uri.fromFile(file);
        return uri;
    }

    public static File getOutputMediaFile(int type) {
        Log.d("symptom", "getOutputMediaFile() type:" + type);

        File mediaStorageDir = new File(
                Environment
                        .getExternalStorageDirectory(),
                IMAGE_CACHE_DIRECTORY);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("symptom", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US)
                .format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + IMAGE_NAME_PREFIX + timeStamp + ".jpg");
        } else {
            Log.e("symptom", "typ of media file not supported: type was:" + type);
            return null;
        }
        return mediaFile;
    }
    public abstract static class MedDialogOnClickListener implements DialogInterface.OnClickListener {
        MedicationTakenDto mMedicationTakenDto;
        public MedDialogOnClickListener(MedicationTakenDto mMedicationTakenDto) {
            this.mMedicationTakenDto = mMedicationTakenDto;
        }
    }
    public void askIfAndWhen(MedicationTakenDto medDto, final int j) {
        String msg = "Did you take your " + medDto.medication.name + "?";
        AlertDialog alertDialog = new AlertDialog.Builder(CheckInActivity.this).setMessage(msg).setNegativeButton(
                "No", new MedDialogOnClickListener(medDto) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        this.mMedicationTakenDto.hasTaken = false;

                    }
                }).setPositiveButton("Yes", new MedDialogOnClickListener(medDto) {
            public void onClick(DialogInterface dialogInterface, int i) {
                this.mMedicationTakenDto.hasTaken = true;
                setTimeSetListener(new TimePickOnSetTimeListener(this.mMedicationTakenDto, mHour, mMinute), j);
                //  hack so I can track different dialogs

                showDialog(TIME_DIALOG_ID + j);
                setDateSetListener(new DatePickOnSetDateListener(this.mMedicationTakenDto, mYear, mMonth, mDay), j);
                showDialog(DATE_DIALOG_ID + j);
            }}).create();
        alertDialog.show();
    }
    public void askPatientAboutTakingMedications() {
               int numMeds = mCheckIn.medicationsTaken.size();

                this.mTimeSetListener = new TimePickOnSetTimeListener[numMeds];
                this.mDateSetListener = new DatePickOnSetDateListener[numMeds];

                for (int i = 0; i < numMeds ; i++) {
                    MedicationTakenDto medicationTakenDto = ((ArrayList<MedicationTakenDto>)mCheckIn.medicationsTaken).get(i);
                    if (medicationTakenDto == null) continue;
                   askIfAndWhen(medicationTakenDto, i);
        }
    }

    public void finishSend(boolean success) {
        if (success) {
            Toast.makeText(this, "Send Successful", Toast.LENGTH_SHORT).show();
        }
        else {
           Toast.makeText(this, "Send unsuccessful", Toast.LENGTH_SHORT).show();
        }
    }

    /*
    Start and Acitivity to take a Photo, and put the filename in the Intent as an extra
     */
    public void takePhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        this.mImageFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
        this.mImageUri = getOutputMediaFileUri(this.mImageFile);
        if (this.mImageUri == null) {
            Toast.makeText(this, "Can't take photo, problem with SD stoarge", Toast.LENGTH_SHORT).show();
        }
        else {
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, this.mImageUri);
            startActivityForResult(takePictureIntent, CAMERA_PIC_REQUEST);
        }
    }
    /* If the Take photo activity is successful, associate the image name with the
    my current CheckIn object.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_PIC_REQUEST) {
            if (resultCode == RESULT_OK) {
                // Since we were successful, associate the name of the Image file with the
                // CheckIn object
                // TODO assert that the filename in the intent matches mCheckIn.imageName

                mCheckIn.imageName = this.mImageUri.toString();
            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the image capture
            } else {
                Toast.makeText(this, "Failed to take photo", Toast.LENGTH_LONG).show();
            }
        }


    }

}
