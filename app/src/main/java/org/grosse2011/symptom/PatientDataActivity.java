package org.grosse2011.symptom;


/* Simple Screen which display a "record" for a patient entity */

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class PatientDataActivity extends Activity {
    String mFirstName;
    String mLastName;
    long mPatientId;
    long mMedicalRecordNo;
    String mBirthDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_data);
        this.mFirstName  = getIntent().getStringExtra(Misc.PATIENT_FIRST_NAME_KEY);
        this.mLastName = getIntent().getStringExtra(Misc.PATIENT_LAST_NAME_KEY);
        this.mPatientId = getIntent().getLongExtra(Misc.PATIENT_ID_KEY, -1);
        this.mMedicalRecordNo = getIntent().getLongExtra(Misc.PATIENT_MEDICAL_RECORD_NO_KEY, -1);
        this.mBirthDate = getIntent().getStringExtra(Misc.PATIENT_BIRTHDATE_KEY);
        setTitle(mLastName + ", " + mFirstName);

        TextView firstNameTextView = (TextView) findViewById(R.id.patientDataFirstName);
        TextView lastNameTextView = (TextView) findViewById(R.id.patientDataLastName);

        TextView medicatlRecordNoTextView = (TextView) findViewById(R.id.patientDataMedicalRecordNo);
        TextView birthDateTextView = (TextView)findViewById(R.id.patientDataBirthDate);
        firstNameTextView.setText("First Name: " + mFirstName);
        lastNameTextView.setText("Last Name: " + mLastName);
        medicatlRecordNoTextView.setText("Medical Record No: " + Long.valueOf(mMedicalRecordNo).toString());
        birthDateTextView.setText("BirthDate: " + mBirthDate);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.patient_data, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
