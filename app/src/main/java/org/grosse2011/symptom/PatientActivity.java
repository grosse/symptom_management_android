package org.grosse2011.symptom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

/*
 Initial activity in patient flow, give patient choice of:
   Checking In
   Edit reminder times
 */

public class PatientActivity extends Activity {
    Context mContext;
    long mPatientId;
    String mPatientLastName;
    String mPatientFirstName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient);
        this.mPatientId = getIntent().getLongExtra(Misc.PATIENT_ID_KEY, -1);
        this.mPatientLastName = getIntent().getStringExtra(Misc.PATIENT_LAST_NAME_KEY);
        this.mPatientFirstName = getIntent().getStringExtra(Misc.PATIENT_FIRST_NAME_KEY);
        this.setTitle("Welcome: " + this.mPatientFirstName + " " + this.mPatientLastName);
        // Textview for each choice
        TextView checkInText = (TextView)findViewById(R.id.PatientCheckInTextView);
        TextView checkInReminder = (TextView)findViewById(R.id.PatientEditCheckInTextView);

        this.mContext = this;
        // Patient is checking In
        checkInText.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, CheckInActivity.class);
                // Store patient data as Extra in Activity intent
                intent.putExtra(Misc.PATIENT_ID_KEY, mPatientId);
                intent.putExtra(Misc.PATIENT_FIRST_NAME_KEY, mPatientFirstName);
                intent.putExtra(Misc.PATIENT_LAST_NAME_KEY, mPatientLastName);
                startActivity(intent);
            }
        });
        // Patient is editing reminder times
        checkInReminder.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, PatientEditReminderTimes.class);
                // Store patient data as Extra in Activity intent
                intent.putExtra(Misc.PATIENT_ID_KEY, mPatientId);
                intent.putExtra(Misc.PATIENT_LAST_NAME_KEY, mPatientLastName);
                intent.putExtra(Misc.PATIENT_FIRST_NAME_KEY, mPatientFirstName);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.patient, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
