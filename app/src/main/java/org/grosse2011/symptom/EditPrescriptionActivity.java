package org.grosse2011.symptom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Callable;

import business_objects.PainMedicationDto;
import business_objects.PatientDto;
import business_objects.PrescriptionDto;
import client.CallableTask;
import client.Callables;
import client.SymptomSvc;
import client.SymptomSvcApi;
import client.TaskCallback;
import oauth.SecuredRestException;
/*

This activity is where the doctor changes a patients prescription
   A complete medications list is downloaded from the spring service
   This list is used to make a list_view and checkboxes are used
   to indicated if the patient is to be prescribed that medication.
   A save button uploads the new prescription to the spring service
   Implements taskCallback for the getAllMeds REST call

 */
/*
This class is implements the TaskCallback interface, TODO make a Callable instead
 */

public class EditPrescriptionActivity extends Activity implements TaskCallback<Collection<PainMedicationDto>> {
    private Context mContext = this;
    private PrescriptionDto newPrescription;
    private long mPatientId;
    private String mLastName;
    private String mFirstName;
    public long getPatientId() { return this.mPatientId; }
    //private boolean removeResult;
    //private boolean addResult;
    public void addToPrescription(String name) { newPrescription.addMedication(name);  }
    public void removeFromPrescription(String name) {
        newPrescription.removeMedication(name);
    }
    public PrescriptionDto getNewPrescription() {
        return this.newPrescription;
    }
    /*
    public void updateCurrentToNewPrescription() {
        this.curPrescription = this.newPrescription;
    }
    */
    private PrescriptionDto mCurPrescription;
    private PrescriptionDto mOrigPrescription;
    private Collection<PainMedicationDto> allMeds;
    private Button mSendButton;

    private final SymptomSvcApi mSvc = SymptomSvc.getCurrentSvc(); //SymptomSvc.init(Misc.SERVER_URL);

    public SymptomSvcApi getSvc() { return this.mSvc; }
    private ListView mListView;
    //public EditPrescriptionActivity activity;

    /* Adapter for displaying pain medications in a ListView */

    public static class PainMedicationsArrayAdapter extends ArrayAdapter<PainMedicationDto> {
        private final Context mContext;
        private final ArrayList<PainMedicationDto> mValues;
        private final EditPrescriptionActivity mActivity;

        public PainMedicationsArrayAdapter(Context context, ArrayList<PainMedicationDto> values, EditPrescriptionActivity activity) {
            super(context, R.layout.edit_prescription_list_item, values);
            this.mContext = context;
            this.mValues = values;
            this.mActivity = activity;
        }


        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            String medName = mValues.get(position).name;
            View rowView = inflater.inflate(R.layout.edit_prescription_list_item, parent, false);
            CheckBox checkBox = (CheckBox) rowView.findViewById(R.id.editPrescriptionCheckBox);
            checkBox.setText(medName);
            /* Make it black TODO somebody with taste should look at this*/
            checkBox.setTextColor(Color.rgb(0x00, 0x00, 0x00));

            assert (this == null);
            assert (this.mActivity.newPrescription != null);
            assert (medName != null);

            if (this.mActivity.newPrescription.containsMedication(medName)) {
                checkBox.setChecked(true);
            } else {
                checkBox.setChecked(false);
            }


            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CheckBox addToPrescriptionCheckBox = (CheckBox) view;
                    if (addToPrescriptionCheckBox.isChecked()) {
                        mActivity.addToPrescription(addToPrescriptionCheckBox.getText().toString());

                    } else {
                        mActivity.removeFromPrescription(addToPrescriptionCheckBox.getText().toString());
                     }
                }
            });
            return rowView;
        }
    }
    public void setPrescription(PrescriptionDto pre) {
        this.mCurPrescription = pre;
        this.newPrescription = this.mCurPrescription;
    }
    //  The method forwards to REST call to get a complete list of pain medications
    // Continuation "Future" after Asynchronous Rest call to GetPainMedications
    private void finishGetPainMedications() {

        this.mListView = (ListView) findViewById(R.id.editPrescriptionsListView);
        this.mOrigPrescription = this.mCurPrescription;
        ArrayAdapter<PainMedicationDto> painMedicationsArrayAdapter = new PainMedicationsArrayAdapter(this.getApplicationContext(), ((ArrayList<PainMedicationDto>) this.allMeds), this);
        mListView.setAdapter(painMedicationsArrayAdapter);
        mSendButton = (Button) findViewById(R.id.editPrescriptionSendButton);

        // Upload the new Prescription to the spring service

        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // REST call to upload prescription to spring service
                CallableTask.invoke(
                        new Callables.upsertPrescriptionForPatient(EditPrescriptionActivity.this)
                        , new TaskCallback<PatientDto>() {
                            @Override
                            public void success(PatientDto result) {
                                Toast.makeText(mContext, "prescription sent", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void error(Exception exception) {
                                if ( exception instanceof SecuredRestException) {
                                    Intent intent = new Intent(EditPrescriptionActivity.this, LoginScreenActivity.class);
                                    startActivity(intent);
                                }
                                else {
                                    Toast.makeText(EditPrescriptionActivity.this, exception.toString(), Toast.LENGTH_SHORT).show();
                                }

                            }
                        });
            }
        });
    }

   /*******************************
   ENTRY Point of Activity is HERE
    *******************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_prescription);
        Intent intent = getIntent();
        this.mPatientId = intent.getLongExtra(Misc.PATIENT_ID_KEY, -1);
        this.mLastName = intent.getStringExtra(Misc.PATIENT_LAST_NAME_KEY);
        this.mFirstName = intent.getStringExtra(Misc.PATIENT_FIRST_NAME_KEY);
        setTitle(this.mLastName + ", " + this.mFirstName);

        // REST call Get the prescription for this patient

        CallableTask.invoke(
                new Callables.getPrescriptionByPatientCallable(mSvc, mPatientId)
                , new TaskCallback<PrescriptionDto>() {

                    @Override
                    public void success(PrescriptionDto prescriptionDto) {
                        setPrescription(prescriptionDto);
                        // Now get a list of all pain medications
                        // TODO optimazation my be to do this Asynchronzously once
                        // at login, and store somewhere
                        // Perhaps acdess as a Singleton

                        getPainMedications();
                    }

                    @Override
                    public void error(Exception e) {
                        if ( e instanceof SecuredRestException) {
                            Intent intent = new Intent(EditPrescriptionActivity.this, LoginScreenActivity.class);
                            startActivity(intent);
                        }
                        else {
                            Toast.makeText(EditPrescriptionActivity.this, "Problem getting patient script", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.edit_prescription, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return (id == R.id.action_settings) ? true : super.onOptionsItemSelected(item);
    }

    // Forward call to Callable Task to get PainMedications from via REST api to Server

    private void getPainMedications() {
        CallableTask.invoke(new Callable<Collection<PainMedicationDto>>() {

            @Override
            public Collection<PainMedicationDto> call() throws Exception {
                return mSvc.getPainMedications();
            }
        }, this);
    }

    // getPainMedications callbackz

    // Successfully received all medications from Server
    @Override
    public void success(Collection<PainMedicationDto> result) {
        this.allMeds = result;
        finishGetPainMedications();
    }
    // Problem getting list of all medicatiins from server
    @Override
    public void error(Exception e) {
        if ( e instanceof SecuredRestException) {
            Intent intent = new Intent(EditPrescriptionActivity.this, LoginScreenActivity.class);
            startActivity(intent);
        }
        else {
            Toast.makeText(EditPrescriptionActivity.this, "Problem getting full medication list", Toast.LENGTH_SHORT).show();
        }
    }
}




