package org.grosse2011.symptom;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import business_objects.ReminderTimes;


/**
 * Created by eric on 11/6/2014.

This is a broadcast Receiver that will receive Alarm notifications for a patient to CheckIn.
 */


public class ReminderReceiver extends BroadcastReceiver {

    public static final int CHECKIN_NOTIFICATION_ID = 1;
    public static final String CHECKIN_NOTIFICATION_TAG = "CheckIn_Notification_TAG";
    private static final String TAG = "AlarmNotificationReceiver";

    // Notification Text Elements
    private final CharSequence tickerText = "Your checkin Time has arrived";
    private final CharSequence contentTitle = "Time To check in";
    private final CharSequence contentText = "Your doctor wants to here from you!!";

    // Notification Action Elements
    private Intent mNotificationIntent;
    private PendingIntent mContentIntent;
    private long mPatiendId;

    private static final long[] mVibratePattern = { 0, 200, 200, 300 };

    /* TODO */

    public static void cancelAlarms(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent updateServiceIntent = new Intent(context, ReminderReceiver.class);
        PendingIntent pendingUpdateIntent = PendingIntent.getService(context, 0, updateServiceIntent, 0);

        // Cancel alarms
        try {
            alarmManager.cancel(pendingUpdateIntent);
        } catch (Exception e) {
            Toast.makeText(context, "I couldn't cancel, sorry", Toast.LENGTH_SHORT).show();

        }
    }
    /* This is a factory method to make a Broadcast Intent */

    public static PendingIntent makeReminderIntent(Context context, long patientId, ReminderTimes.ReminderTime reminderTime) {

        Intent reminderIntent = new Intent(context, ReminderReceiver.class);
        reminderIntent.putExtra(Misc.PATIENT_ID_KEY, patientId);
        reminderIntent.putExtra(Misc.REMINDER_MINUTE_KEY, reminderTime.minute);
        reminderIntent.putExtra(Misc.REMINDER_HOUR_KEY, reminderTime.hour);
        return PendingIntent.getBroadcast(context, 0, reminderIntent, PendingIntent.FLAG_UPDATE_CURRENT);

    }
    /* Here is the hook method where we receive the Intent */

    @Override
    public void onReceive(Context context, Intent intent) {


        long patientId = intent.getLongExtra(Misc.PATIENT_ID_KEY, -1);

        // build the intent for Notification
        mNotificationIntent = new Intent(context, CheckInActivity.class);
        mNotificationIntent.putExtra(Misc.PATIENT_ID_KEY, patientId);
        // The PendingIntent that wraps the underlying Intent
        mContentIntent = PendingIntent.getActivity(context, 0,
                mNotificationIntent, PendingIntent.FLAG_ONE_SHOT);

        // Build the Notification

        ReminderTimes.ReminderTime nextReminderTime = ReminderTimes.nextTime(context, patientId);
        Notification.Builder notificationBuilder = new Notification.Builder(
                context).setTicker(tickerText)
                .setSmallIcon(android.R.drawable.stat_sys_warning)
                .setAutoCancel(true).setContentTitle(contentTitle)
                .setContentText(contentText).setContentIntent(mContentIntent)
                .setVibrate(mVibratePattern);

        // Get the NotificationManager
        NotificationManager mNotificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);


        AlarmManager mAlarm = (AlarmManager)context.getSystemService(context.ALARM_SERVICE);
        // TODO should cancel alarms but not in requirements
        //  get a new intent to broadcast
        PendingIntent reminderPendingIntent = makeReminderIntent(context, patientId, nextReminderTime);
        // 1) Set alarm for next reminder
        mAlarm.set(AlarmManager.RTC_WAKEUP, ReminderTimes.nextTime(context, patientId).millis(), reminderPendingIntent);

        // 2) Send a notification
        mNotificationManager.notify(CHECKIN_NOTIFICATION_TAG, CHECKIN_NOTIFICATION_ID,
                notificationBuilder.build());
    }
}


