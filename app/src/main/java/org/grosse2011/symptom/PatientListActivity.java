package org.grosse2011.symptom;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Callable;
import business_objects.PatientDto;
import client.CallableTask;
import client.SymptomSvc;
import client.SymptomSvcApi;
import client.TaskCallback;



/* This activity used by a doctor display a list of this doctors paitnets
   The doctor can click on a patient and be sent to the PatientOpsActivity
   for that patient
*/

public class PatientListActivity extends ListActivity implements TaskCallback<Collection<PatientDto>> {
    ArrayList<PatientDto> mPatientDtos;
    Context mContext;
    private long mDoctorId = -1;
    private String mFirstName;
    private String mLastName;

    private final SymptomSvcApi svc = SymptomSvc.getCurrentSvc();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mDoctorId = getIntent().getLongExtra(Misc.DOCTOR_ID_KEY, -1);
        this.mFirstName = getIntent().getStringExtra(Misc.DOCTOR_FIRST_NAME_KEY);
        this.mLastName = getIntent().getStringExtra(Misc.DOCTOR_LAST_NAME_KEY);

        setTitle("Patients of Dr. " + this.mFirstName + " " + this.mLastName);
        CallableTask.invoke(new Callable<Collection<PatientDto>>() {

            @Override
            public Collection<PatientDto> call() throws Exception {
                return svc.getPatientsByDoctor(mDoctorId);
            }
        }, this);

    }
    private void finishUp() {

        ArrayAdapter<PatientDto>  adapter = new ArrayAdapter<PatientDto> (this, R.layout.patient_list_item, this.mPatientDtos);
        setListAdapter(adapter);
        mContext = this;

        ListView listView = getListView();
        listView.setTextFilterEnabled(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                                                PatientDto patient = mPatientDtos.get(position);
                                                Intent intent = new Intent(mContext, PatientOpsActivity.class);
                                                intent.putExtra(Misc.PATIENT_ID_KEY, patient.getId());
                                                intent.putExtra(Misc.PATIENT_FIRST_NAME_KEY, patient.firstName);
                                                intent.putExtra(Misc.PATIENT_LAST_NAME_KEY, patient.lastName);
                                                intent.putExtra(Misc.PATIENT_MEDICAL_RECORD_NO_KEY, patient.medicalRecordNo);
                                                intent.putExtra(Misc.PATIENT_BIRTHDATE_KEY, patient.birthDateAsString());
                                                startActivity(intent);
                                                                                            }
                                        }
        );
    }


    public void success(Collection<PatientDto> result) {
        String s;
        this.mPatientDtos = (ArrayList<PatientDto>)result;
        finishUp();
      }

    @Override
    public void error(Exception e) {

    }
}
