package org.grosse2011.symptom;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;
import java.util.concurrent.Callable;
import business_objects.DoctorDto;
import business_objects.PatientDto;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import client.CallableTask;
import client.SymptomSvc;
import client.SymptomSvcApi;
import client.TaskCallback;


/**
 *
 * This application uses ButterKnife.
 *  This is the login screen activity.  Our app serves both client and patients to make
 *  this project easier logistically.  If the username is of the form doctorn we'll go
 *  throught the doctor acxtivity flow, if it is a patientn we'll do the patient activity
 *  flow
 *
 *  Eric
 */
public class LoginScreenActivity extends Activity {

    @InjectView(R.id.userName)
    protected EditText userName_;

    @InjectView(R.id.password)
    protected EditText password_;

    @InjectView(R.id.server)
    protected EditText server_;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        ButterKnife.inject(this);
    }

    @OnClick(R.id.loginButton)
    public void login() {
        final String user = userName_.getText().toString();
        String pass = password_.getText().toString();
        String server = server_.getText().toString();


        final SymptomSvcApi svc = SymptomSvc.init(server, user, pass);

        final long doctorId, patientId;
        /* doctors have id's of the doctors where n is an integer.  n=id in data store.
        This hard coded behavior should be changed.
        */
        if (user.startsWith("doctor")) {
        try {
            // Get the integer after "doctor"
            doctorId = Long.parseLong(user.substring(user.lastIndexOf("r") + 1));
        } catch(Exception e) {
            Toast.makeText(LoginScreenActivity.this, "That is an invalid patient username", Toast.LENGTH_SHORT).show();
            return ;
        }


        CallableTask.invoke(new Callable<DoctorDto>() {

                @Override
                public DoctorDto call() throws Exception {
                    return svc.getDoctor(doctorId);
                }
            }, new TaskCallback<DoctorDto>() {

                //  @Override
                public void success(DoctorDto result) {
                    if (result != null) {
                        // Success, got doctor info, oauth2 worked
                        startDoctorActivity(result);
                    } else {
                        Toast.makeText(LoginScreenActivity.this, "That is an invalid doctor username", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void error(Exception e) {
                    Log.e(LoginScreenActivity.class.getName(), "Error logging in via OAuth.", e);

                    Toast.makeText(
                            LoginScreenActivity.this,
                            "Login failed, check your Internet connection and credentials.",
                            Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            /* if its not a doctor, should be a patient
               patients have id's of the from patientn where n is an integer.  n=id in data store.
               This hard coded behavior should be changed.
             */
            if (user.startsWith("patient")) {
                try {
                    // Get integer after "patient"
                     patientId = Long.parseLong(user.substring(user.lastIndexOf("t") + 1));
                } catch(Exception e) {
                    Toast.makeText(LoginScreenActivity.this, "That is an invalid patient username", Toast.LENGTH_SHORT).show();
                    return ;
                }
                // Call to web server to get patient information
                CallableTask.invoke(new Callable<PatientDto>() {

                    @Override
                    public PatientDto call() throws Exception {
                        return svc.getPatient(patientId);
                    }
                }, new TaskCallback<PatientDto>() {

                    //  @Override
                    public void success(PatientDto result) {
                        if (result != null) {
                            // success Got patient info -- oauth2 worked
                            startPatientActivity(result);
                        } else {
                            Toast.makeText(LoginScreenActivity.this, "That is an invalid patient username", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void error(Exception e) {
                        Log.e(LoginScreenActivity.class.getName(), "Error logging in via OAuth.", e);

                        Toast.makeText(
                                LoginScreenActivity.this,
                                "Login failed, check your Internet connection and credentials.",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                Toast.makeText(LoginScreenActivity.this, "Invalid userame", Toast.LENGTH_SHORT).show();
            }

        }
    }
    /*
    This user is a doctor, start the doctor activity.
     */
    public void startDoctorActivity(DoctorDto doctorDto) {
        Intent intent = new Intent(LoginScreenActivity.this, DoctorsOfficeActivity.class);
        // Package doctor's information as Extra's into intent
        intent.putExtra(Misc.DOCTOR_ID_KEY, doctorDto.id);
        intent.putExtra(Misc.DOCTOR_FIRST_NAME_KEY, doctorDto.firstName);
        intent.putExtra(Misc.DOCTOR_LAST_NAME_KEY, doctorDto.lastName);
        startActivity(intent);
    }
    /*
    This user is a patient, start the patient activity.
     */
    public void startPatientActivity(PatientDto patientDto) {
        Intent intent = new Intent(LoginScreenActivity.this, PatientActivity.class);
        // Package patient information as Extra's into intent
        intent.putExtra(Misc.PATIENT_ID_KEY, patientDto.getId());
        intent.putExtra(Misc.PATIENT_LAST_NAME_KEY, patientDto.lastName);
        intent.putExtra(Misc.PATIENT_FIRST_NAME_KEY, patientDto.firstName);
        startActivity(intent);
    }

}



