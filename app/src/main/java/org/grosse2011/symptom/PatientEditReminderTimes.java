package org.grosse2011.symptom;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import business_objects.ReminderTimes;

/*
This Activity allows the patient to change the times they get reminders
 */

public class PatientEditReminderTimes extends Activity {
    private final int TIME_DIALOG_ID = 0;
    private ListView mReminderTimesListView;
    private Button mSaveReminderTimeButton;
    private Button mAddReminderTimeButton;
    private AlarmManager mAlarm;
    private long mPatientId;
    private String mFirstName;
    private String mLastName;

    ArrayAdapter<String> mAdapter;

    private Context mContext;

    private ArrayList<ReminderTimes.ReminderTime> mReminderTimes; // = new ArrayList<ReminderTimes.ReminderTime>();

    public void setReminderTimes(ArrayList<ReminderTimes.ReminderTime> reminderTimes) {
        Collections.sort(reminderTimes);
        this.mReminderTimes = reminderTimes;
    }

    public ArrayList<ReminderTimes.ReminderTime> getReminderTimes() {
        return this.mReminderTimes;
    }

    /* Array adapter of checkboxes for reminder times editing
      Reminder times are displayed as checkBoxes, checked indicates this
      Reminder time is currently active
    */
    public static class PatientTimesArrayAdapter extends ArrayAdapter<String> {
        private final Context mContext;
        private final PatientEditReminderTimes mActivity;
        public PatientTimesArrayAdapter(Context context, PatientEditReminderTimes activity) {
            super(context, R.layout.edit_reminder_times_list_item);
            this.mActivity = activity;
            this.mContext = context;
        }

    //public int getReminderTimeCount() {
            // TODO Auto-generated method stub
     //  return this.mActivity.getReminderTimes().size();
    //   }

    public View getView(final int position, View convertView, ViewGroup parent) {
         LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         String name = this.mActivity.getReminderTimes().get(position).toString();

            View rowView = inflater.inflate(R.layout.edit_reminder_times_list_item, parent, false);
            CheckBox reminderTimeCheckBox = (CheckBox) rowView.findViewById(R.id.editReminderTimesCheckBox);
            Button removeReminderTimeButton = (Button) rowView.findViewById(R.id.editReminderTimesRemoveButton);
            reminderTimeCheckBox.setText(name);
            reminderTimeCheckBox.setTextColor(Color.rgb(0x00, 0x00, 0x00));
            removeReminderTimeButton.setTextColor(Color.rgb(0x00, 0x00, 0x00));

            // Change sense of "check"

            if (this.mActivity.getReminderTimes().get(position).isEnabled) {
                reminderTimeCheckBox.setChecked(true);
            } else {
                reminderTimeCheckBox.setChecked(false);
            }


            removeReminderTimeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ReminderTimes.ReminderTime reminder = mActivity.getReminderTimes().get(position);
                    // We can remove a reminder if it is not currently enable, or if it is enabled,
                    // Subtracting this reminder will not leave to few enabled reminders

                    if (!reminder.isEnabled || (reminder.isEnabled && ReminderTimes.enoughEnabledReminders(mActivity.getReminderTimes()))) {
                        mActivity.getReminderTimes().remove(position);
                        ReminderTimes.saveRemindersToPrefs(mActivity, mActivity.getReminderTimes(), mActivity.mPatientId);
                        notifyDataSetChanged();
                    } else {
                        // Can't remove this reminder, there are too few left
                        Toast.makeText(mActivity, "You need at least " + ReminderTimes.MIN_REMINDERS + " Reminders\nPlease add and enable some reminder", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            /*
              Let user check and uncheck reminder times, but
              save results to user shared preferences
              don't let user have less than min or more than max checked

             */
            reminderTimeCheckBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CheckBox thisCheckBox = (CheckBox) view;
                    if (thisCheckBox.isChecked()) {
                        if (!ReminderTimes.fullOfEnabled(mActivity.getReminderTimes())) {
                            mActivity.getReminderTimes().get(position).isEnabled = true;
                            ReminderTimes.saveRemindersToPrefs(mActivity, mActivity.getReminderTimes(), mActivity.mPatientId);

                        } else {
                            thisCheckBox.setChecked(false);
                            Toast.makeText(mActivity, "You cannot have more than " + ReminderTimes.MAX_REMINDERS + " Reminders\nPlease disable at least 1 reminder", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (ReminderTimes.enoughEnabledReminders(mActivity.getReminderTimes())) {
                            mActivity.getReminderTimes().get(position).isEnabled = false;
                            ReminderTimes.saveRemindersToPrefs(mActivity, mActivity.getReminderTimes(), mActivity.mPatientId);
                        } else {
                            thisCheckBox.setChecked(true);
                            if (ReminderTimes.enoughReminders(mActivity.getReminderTimes())) {
                                Toast.makeText(mActivity, "You need at least " + ReminderTimes.MIN_REMINDERS + " Reminders.\nPlease enable some reminder", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(mActivity, "You need at least " + ReminderTimes.MIN_REMINDERS + " Reminders\nPlease add and enable some reminder", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            });
            return rowView;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAlarm = (AlarmManager) this.getSystemService(ALARM_SERVICE);
        setContentView(R.layout.activity_patient_edit_reminder_times);
        this.mContext = this;
        this.mPatientId = getIntent().getLongExtra(Misc.PATIENT_ID_KEY, -1);
        this.mLastName = getIntent().getStringExtra(Misc.PATIENT_LAST_NAME_KEY);
        this.mFirstName = getIntent().getStringExtra(Misc.PATIENT_FIRST_NAME_KEY);
        setTitle(mFirstName + " "  + mLastName);
        this.mReminderTimesListView = (ListView) findViewById(R.id.editReminderTimesListView);
        this.mSaveReminderTimeButton = (Button) findViewById(R.id.editReminderTimesSaveButton);
        this.setReminderTimes(ReminderTimes.loadRemindersFromPrefs(this, mPatientId));
        this.mAddReminderTimeButton = (Button) findViewById(R.id.editReminderTimesAddButton);
        /* save button:
         Cancel old alarms
         Set up a new reminder pendingIntnet
          Set new alarm to fire new reminder pendingIntent
         */

        this.mSaveReminderTimeButton.setOnClickListener(new View.OnClickListener() {
                                                 @Override
                                                public void onClick(View view) {
                                                   // ReminderReceiver.cancelAlarms(PatientEditReminderTimes.this);
                                                    ReminderTimes.ReminderTime nextReminderTime = ReminderTimes.nextTime(PatientEditReminderTimes.this, mPatientId);
                                                    PendingIntent reminderPendingIntent = ReminderReceiver.makeReminderIntent(PatientEditReminderTimes.this, mPatientId, nextReminderTime);
                                                    Toast.makeText(PatientEditReminderTimes.this, "Your next reminder will be at " + ReminderTimes.nextTime(PatientEditReminderTimes.this, mPatientId), Toast.LENGTH_SHORT).show();
                                                    mAlarm.set(AlarmManager.RTC_WAKEUP, nextReminderTime.millis(), reminderPendingIntent);
                                                }
                                            }
        );
        /* Add button allows user
           to create a new reminder time

         */
        this.mAddReminderTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(TIME_DIALOG_ID);
            }
        });
        this.mAdapter = new PatientTimesArrayAdapter(this.getApplicationContext(), this);
        this.mReminderTimesListView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.patient_edit_reminder_times, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    /* User can make Add new Reminder times using a TimePicker dialong */

    protected Dialog onCreateDialog(int id) {
        if (id == TIME_DIALOG_ID) {
            Calendar calendar = Calendar.getInstance();
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            return new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    if (view.isShown()) {
                        ReminderTimes.ReminderTime reminderTime = new ReminderTimes.ReminderTime(hourOfDay, minute, false);
                        ArrayList<ReminderTimes.ReminderTime> reminderTimes = PatientEditReminderTimes.this.mReminderTimes;
                        if (!reminderTimes.contains(reminderTime)) {
                            reminderTimes.add(reminderTime);
                            // Save this to User's preferences file
                            ReminderTimes.saveRemindersToPrefs(PatientEditReminderTimes.this, reminderTimes, mPatientId);
                            // redisplay updated list
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }, hour, minute, false);
        }
        return null;
    }
}


