package org.grosse2011.symptom;

/**
 * Created by eric on 11/16/2014.
 */

/* Class used to track the current state of an image download */

public class ImageStatus {

    public enum ImageState {
        READY, PROCESSING
    }

    private ImageState state;

    public ImageStatus(ImageState state) {
        super();
        this.state = state;
    }

    public ImageState getState() {
        return state;
    }

    public void setState(ImageState state) {
        this.state = state;
    }

}
