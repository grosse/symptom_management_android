
package org.grosse2011.symptom;


import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import business_objects.PatientDto;

/* This is a Doctor's initial screen
 Gives the doctor a choice between viewing his paitient list, or a
 list of her CheckIns*/


public class DoctorsOfficeActivity extends ListActivity {
    ArrayList<PatientDto> patients;
    Context context;
    public final String VIEW_PHOTO_TEXT = "View photo";
    public final String VIEW_ALERTS_TEXT = "View Alerts";
    public final String VIEW_PATIENTS_TEXT = "View Patient List";
    long mCheckInId;
    String mLastName;
    String mFirstName;
    Long mDoctorId;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getApplicationContext();

        this.mFirstName = getIntent().getStringExtra(Misc.DOCTOR_FIRST_NAME_KEY);
        this.mLastName = getIntent().getStringExtra(Misc.DOCTOR_LAST_NAME_KEY);
        this.mDoctorId = getIntent().getLongExtra(Misc.DOCTOR_ID_KEY, -1);
        setTitle(this.mFirstName + " " + this.mLastName + " M.D.");

        ArrayList<String> operations = new ArrayList<String>();
        operations.add(VIEW_ALERTS_TEXT);
        operations.add(VIEW_PATIENTS_TEXT);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.doctors_office_list_item, operations);
        setListAdapter(adapter);
        ListView listView = getListView();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String curText = ((TextView)view).getText().toString();
                if (curText.equals(VIEW_ALERTS_TEXT)){
                    Intent intent = new Intent(context, AlertListActivity.class);
                    intent.putExtra(Misc.DOCTOR_ID_KEY, mDoctorId);
                    intent.putExtra(Misc.DOCTOR_FIRST_NAME_KEY, mFirstName);
                    intent.putExtra(Misc.DOCTOR_LAST_NAME_KEY, mLastName);
                    startActivity(intent);
                    return;
                }
                if (curText.equals("View Patient List")) {
                    Intent intent = new Intent(context, PatientListActivity.class);
                    intent.putExtra(Misc.DOCTOR_ID_KEY, mDoctorId);
                    intent.putExtra(Misc.DOCTOR_FIRST_NAME_KEY, mFirstName);
                    intent.putExtra(Misc.DOCTOR_LAST_NAME_KEY, mLastName);
                    startActivity(intent);
                    return;
                }
            }
        });


    }
}





