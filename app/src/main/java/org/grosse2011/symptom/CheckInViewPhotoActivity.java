package org.grosse2011.symptom;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import client.CallableTask;
import client.Callables;
import client.SymptomSvc;
import client.SymptomSvcApi;
import client.TaskCallback;

/* This Activity downloads and saves image associated with a CheckIn,
then displays it in an Image view.
 */
public class CheckInViewPhotoActivity extends Activity {

    public final static String IMAGE_DIRECTORY_NAME="symptom_doctor";
    public final static String IMAGE_FILENAME_PREFIX="IMG_CHECKIN_";
    long mCheckInId;
    private final SymptomSvcApi mSvc = SymptomSvc.getCurrentSvc(); // SymptomSvc.init(Misc.SERVER_URL);
    Bitmap mBitmap;
    String mfirstName;
    String mLastName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in_view_photo);
        this.mCheckInId = getIntent().getLongExtra(Misc.CHECKIN_ID_KEY, -1);
        this.mfirstName  = getIntent().getStringExtra(Misc.PATIENT_FIRST_NAME_KEY);
        this.mLastName = getIntent().getStringExtra(Misc.PATIENT_LAST_NAME_KEY);
        if (mfirstName != null && mLastName != null) setTitle("Photo for: " + mLastName + ", " + mfirstName);
        if (this.mCheckInId == -1) {
            Toast.makeText(this, "Something seriously wrong, checkId was not bundled", Toast.LENGTH_SHORT).show();
            return ;
        }
        downloadImage();
    }

    public void downloadImage() {

            //    TypedFile typedImage = new TypedFile("image/jpeg", activity.getImageFile());
                // Call rest API to download file for checkIn, save it, and display in in a continuation
                CallableTask.invoke(
                        new Callables.getImageForCheckIn(this.mCheckInId, this.mSvc)
                        , new TaskCallback<InputStream>() {

                            @Override
                            public void success(InputStream imageData) {
                                byte[] retrievedFile = null;
                                try {
                                    retrievedFile = IOUtils.toByteArray(imageData);
                                } catch (IOException e) {
                                    unsuccessfulDownload();
                                }
                                if (retrievedFile.length > 0) {

                                    String targetFileName = getOutputMediaFile(mCheckInId);
                                    if (targetFileName == null) {
                                        unsuccessfulDownload();
                                    }
                                    File targetFile = new File(targetFileName);
                                    OutputStream outStream = null;
                                    try {
                                        outStream = new FileOutputStream(targetFile);
                                    } catch (FileNotFoundException e) {
                                        unsuccessfulDownload();
                                    }
                                    if (outStream == null) finishDownload();
                                    try {
                                        outStream.write(retrievedFile);
                                        outStream.close();
                                    } catch (IOException e) {
                                        unsuccessfulDownload();
                                    }
                                    mBitmap = BitmapFactory.decodeFile(targetFileName);
                                    finishDownload();
                                } else {
                                    // length < 1
                                    unsuccessfulDownload();
                                }
                            }

                            @Override
                            public void error(Exception e) {
                                unsuccessfulDownload();
                            }
                        });

            }
    // This is a continuation "Future" from AyncTask of downloading file if succesfjul
    public void finishDownload() {
            Toast.makeText(this, "Downloaded image", Toast.LENGTH_SHORT).show();
            ImageView viewPhotoImageView = (ImageView)findViewById(R.id.viewPhotoImageView);
            viewPhotoImageView.setImageBitmap(mBitmap);
     }
    // This is a continuation "Future" from Async task of downloading file if error occurred
    public void unsuccessfulDownload() {
        Toast.makeText(this, "Problem downloading image", Toast.LENGTH_SHORT).show();
    }
    public static String getOutputMediaFile(long checkInId) {
        Log.d("symptom", "getOutputMediaFile()" + checkInId);

        File mediaStorageDir = new File(
                Environment
                        .getExternalStorageDirectory(),
                IMAGE_DIRECTORY_NAME);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("symptom", "failed to create directory");
                return null;
            }
        }

            return mediaStorageDir.getPath() + File.separator
                    + IMAGE_FILENAME_PREFIX + checkInId +  ".jpg";

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.check_in_view_photo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
