package org.grosse2011.symptom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

/* This is a Doctors screen which gives him a choice of operations he can "perform" on
  a Patients Data.
  These operations include:

  1) View Checkins
  2) Edit Prescription
  3) View Patient's data

 */

public class PatientOpsActivity extends Activity {

    String mLastName;
    String mFirstName;
    String mBirthDate;
    long mMedicalRecordNo;
    long mPatientId;
    TextView mHandlePatientPrescriptionTextView;
    TextView mHandlePatientDataTextView;
    TextView mHandlePatientCheckinsTextView;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_ops);

        this.mFirstName  = getIntent().getStringExtra(Misc.PATIENT_FIRST_NAME_KEY);
        this.mLastName = getIntent().getStringExtra(Misc.PATIENT_LAST_NAME_KEY);
        this.mPatientId = getIntent().getLongExtra(Misc.PATIENT_ID_KEY, -1);
        this.mMedicalRecordNo = getIntent().getLongExtra(Misc.PATIENT_MEDICAL_RECORD_NO_KEY, -1);
        this.mBirthDate = getIntent().getStringExtra(Misc.PATIENT_BIRTHDATE_KEY);
        this.mContext = this;
        this.setTitle(this.mLastName + "," + this.mFirstName);
        mHandlePatientDataTextView = (TextView)findViewById(R.id.patientOpsViewPatientData);
        mHandlePatientDataTextView.setText("View patient");
        mHandlePatientPrescriptionTextView = (TextView)findViewById(R.id.patientOpsPrescription);
        mHandlePatientPrescriptionTextView.setText("View/Edit prescription");
        mHandlePatientCheckinsTextView = (TextView)findViewById(R.id.patientOpsViewCheckIns);
        mHandlePatientCheckinsTextView.setText("View check ins");

        mHandlePatientCheckinsTextView.setOnClickListener(new View.OnClickListener() {
           @Override
            public void onClick(View view) {
              Intent intent = new Intent(mContext, CheckInListActivity.class);
              intent.putExtra(Misc.PATIENT_FIRST_NAME_KEY, mFirstName);
              intent.putExtra(Misc.PATIENT_LAST_NAME_KEY, mLastName);
              intent.putExtra(Misc.PATIENT_ID_KEY, mPatientId);
              startActivity(intent);
          }
        });
        mHandlePatientPrescriptionTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, EditPrescriptionActivity.class);
                intent.putExtra(Misc.PATIENT_FIRST_NAME_KEY, mFirstName);
                intent.putExtra(Misc.PATIENT_LAST_NAME_KEY, mLastName);
                intent.putExtra(Misc.PATIENT_ID_KEY, mPatientId);
                startActivity(intent);

            }
        });
        mHandlePatientDataTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, PatientDataActivity.class);
                intent.putExtra(Misc.PATIENT_FIRST_NAME_KEY, mFirstName);
                intent.putExtra(Misc.PATIENT_LAST_NAME_KEY, mLastName);
                intent.putExtra(Misc.PATIENT_MEDICAL_RECORD_NO_KEY, mMedicalRecordNo);
                intent.putExtra(Misc.PATIENT_BIRTHDATE_KEY, mBirthDate);
                intent.putExtra(Misc.PATIENT_ID_KEY, mPatientId);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.patient_ops, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
